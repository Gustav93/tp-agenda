package modelo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import dto.DireccionDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.ReportePersonaDTO;
import dto.TipoDeContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DireccionDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoDeContactoDAO;


public class Agenda 
{
	private PersonaDAO persona;	
	private LocalidadDAO localidad;
	private DireccionDAO direccion;
	private TipoDeContactoDAO tipoDeContacto;
	
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
		this.direccion = metodo_persistencia.createDireccionDAO();
		this.tipoDeContacto = metodo_persistencia.createTipoDeContactoDAO();
	}
	
	// Persona
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public void editarPersona(PersonaDTO persona) 
	{
		this.persona.update(persona);
	}
	
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
	
	public PersonaDTO obtenerDatosPersona(Integer idPersona) 
	{
		return this.persona.obtener(idPersona);
	}
	
	// Localidad
	public void agregarLocalidad(LocalidadDTO l)
	{
		this.localidad.insert(l);
	}

	public void borrarLocalidad(LocalidadDTO l) 
	{
		this.localidad.delete(l);
	}
	
	public void editarLocalidad(LocalidadDTO l) 
	{
		this.localidad.update(l);
	}
	
	public List<LocalidadDTO> obtenerLocalidades()
	{
		return this.localidad.readAll();		
	}
	
	public LocalidadDTO obtenerDatosLocalidad(Integer id) 
	{
		return this.localidad.obtener(id);
	}
	
	public LocalidadDTO obtenerIdLocalidad(String nombre)
	{
		return this.localidad.obtenerID(nombre);
	}
	
	// Direccion
	public void agregarDireccion(DireccionDTO d)
	{
		this.direccion.insert(d);
	}

	public void borrarDireccion(DireccionDTO d) 
	{
		this.direccion.delete(d);
	}
	
	public void editarDireccion(DireccionDTO d) 
	{
		this.direccion.update(d);
	}
	
	public List<DireccionDTO> obteneDirecciones()
	{
		return this.direccion.readAll();		
	}
	
	public DireccionDTO obtenerDatosDireccion(Integer id) 
	{
		return this.direccion.obtener(id);
	}
	
	public Integer obtenerMaximoId()
	{
		return this.direccion.maximoId();
	}
	
	// TipoDeContacto
	public void agregarTipoDeContacto(TipoDeContactoDTO t)
	{
		this.tipoDeContacto.insert(t);
	}

	public void borrarTipoDeContacto(TipoDeContactoDTO t) 
	{
		this.tipoDeContacto.delete(t);
	}
	
	public void editarTipoDeContacto(TipoDeContactoDTO t) 
	{
		this.tipoDeContacto.update(t);
	}
	
	public List<TipoDeContactoDTO> obteneTipoDeContactos()
	{
		return this.tipoDeContacto.readAll();		
	}
	
	public TipoDeContactoDTO obtenerDatosTipoDeContacto(Integer id) 
	{
		return this.tipoDeContacto.obtener(id);
	}
	
	public TipoDeContactoDTO obtenerIdTipoDeContacto(String nombre)
	{
		return this.tipoDeContacto.obtenerID(nombre);
	}
	
}
