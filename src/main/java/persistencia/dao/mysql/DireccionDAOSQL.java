package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.DireccionDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DireccionDAO;

public class DireccionDAOSQL implements DireccionDAO {
	private static final String insert = "INSERT INTO direcciones(calle, altura, piso, depto, idLocalidad) VALUES(?, ?, ?, ?, ?)";
	private static final String delete = "DELETE FROM direcciones WHERE idDireccion = ?";
	private static final String update = "UPDATE direcciones SET calle = ?, altura = ?, piso = ?, depto = ?, idLocalidad = ? WHERE idDireccion = ?";
	private static final String readAll = "SELECT * FROM direcciones";
	private static final String readOne = readAll + " where idDireccion = ?";
	private static final String maximoId = "SELECT IFNULL(MAX(idDireccion), 0) AS resultado FROM direcciones";

	@Override
	public boolean insert(DireccionDTO d) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setString(1, d.getCalle());
			statement.setString(2, d.getAltura());
			statement.setString(3, d.getPiso());
			statement.setString(4, d.getDepto());
			statement.setInt(5, d.getIdLocalidad());
			if(statement.executeUpdate() > 0)
			{
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	@Override
	public boolean delete(DireccionDTO d) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(d.getIdDireccion()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	@Override
	public boolean update(DireccionDTO d) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(update);
			statement.setString(1, d.getCalle());
			statement.setString(2, d.getAltura());
			statement.setString(3, d.getPiso());
			statement.setString(4, d.getDepto());
			statement.setInt(5, d.getIdLocalidad());
			statement.setInt(6, d.getIdDireccion());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	@Override
	public List<DireccionDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<DireccionDTO> direcciones = new ArrayList<DireccionDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAll);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				direcciones.add(getDireccionDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return direcciones;
	}

	@Override
	public DireccionDTO obtener(Integer id) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readOne);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
		
			if(resultSet.next()) 
			{
				return this.getDireccionDTO(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	private DireccionDTO getDireccionDTO(ResultSet resultSet) throws SQLException {
		Integer idDireccion = resultSet.getInt("idDireccion");
		String calle = resultSet.getString("calle");
		String altura = resultSet.getString("altura");
		String piso = resultSet.getString("piso");
		String depto = resultSet.getNString("depto");
		Integer idLocalidad = resultSet.getInt("idLocalidad");
		DireccionDTO d =  new DireccionDTO(calle, altura, piso, depto, idLocalidad);
		d.setIdDireccion(idDireccion);
		return d;
	}
	
	public Integer maximoId()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(maximoId);
			resultSet = statement.executeQuery();
		
			if(resultSet.next())
				return resultSet.getInt("resultado");
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return -1;
	}
	
}
