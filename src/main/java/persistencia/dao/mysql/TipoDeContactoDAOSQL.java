package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TipoDeContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoDeContactoDAO;

public class TipoDeContactoDAOSQL implements TipoDeContactoDAO {
	
	private static final String insert = "INSERT INTO tipoDeContactos(nombre) VALUES(?)";
	private static final String delete = "DELETE FROM tipoDeContactos WHERE idTipoDeContacto = ?";
	private static final String update = "UPDATE tipoDeContactos SET nombre = ? WHERE idTipoDeContacto = ?";
	private static final String readAll = "SELECT * FROM tipoDeContactos";
	private static final String readOne = readAll + " where idTipoDeContacto = ?";
	private static final String readId = readAll + " where nombre = ?";

	@Override
	public boolean insert(TipoDeContactoDTO t) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setString(1, t.getNombre());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	@Override
	public boolean delete(TipoDeContactoDTO t) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(t.getIdTipoDeContacto()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	@Override
	public boolean update(TipoDeContactoDTO t) 
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(update);
			statement.setString(1, t.getNombre());
			statement.setInt(2, t.getIdTipoDeContacto());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}

	@Override
	public List<TipoDeContactoDTO> readAll() 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<TipoDeContactoDTO> tipos = new ArrayList<TipoDeContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAll);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				tipos.add(getTipoDeContactoDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tipos;
	}

	private TipoDeContactoDTO getTipoDeContactoDTO(ResultSet resultSet) throws SQLException 
	{
		Integer idTipoDeContacto = resultSet.getInt("idTipoDeContacto");
		String nombre = resultSet.getString("nombre");
		TipoDeContactoDTO t = new TipoDeContactoDTO(nombre);
		t.setIdTipoDeContacto(idTipoDeContacto);
		return t;
	}

	@Override
	public TipoDeContactoDTO obtener(Integer id) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readOne);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
		
			if(resultSet.next()) 
			{
				return this.getTipoDeContactoDTO(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public TipoDeContactoDTO obtenerID(String nombre) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readId);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();
		
			if(resultSet.next()) 
			{
				return this.getTipoDeContactoDTO(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return null;
	}

}
