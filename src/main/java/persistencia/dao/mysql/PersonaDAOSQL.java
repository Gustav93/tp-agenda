package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(nombre, apellido, fechaNac, telefono, email, idDireccion, idTipoDeContacto)"
										+" VALUES(?, ?, ?, ?, ?, ?, ?)";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String update = "UPDATE personas SET nombre = ?, apellido = ?, fechaNac = ?, telefono = ?, email = ?,"
										+" idDireccion = ?, idTipoDeContacto = ? WHERE idPersona = ?";
	private static final String readAll = "SELECT * FROM personas";
	private static final String readOne = readAll + " where idPersona = ?";
		
	public boolean insert(PersonaDTO p)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setString(1, p.getNombre());
			statement.setString(2, p.getApellido());
			statement.setDate(3, p.getFechaNac());
			statement.setString(4, p.getTelefono());
			statement.setString(5, p.getEmail());
			statement.setInt(6, p.getIdDireccion());
			statement.setInt(7, p.getIdTipoContacto());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean delete(PersonaDTO p)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(p.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public boolean update(PersonaDTO p)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(update);
			statement.setString(1, p.getNombre());
			statement.setString(2, p.getApellido());
			statement.setDate(3, p.getFechaNac());
			statement.setString(4, p.getTelefono());
			statement.setString(5, p.getEmail());
			statement.setInt(6, p.getIdDireccion());
			statement.setInt(7, p.getIdTipoContacto());
			statement.setInt(8, p.getIdPersona());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAll);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
	
	@Override
	public PersonaDTO obtener(Integer id) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readOne);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
		
			if(resultSet.next()) 
			{
				return this.getPersonaDTO(resultSet);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException
	{
		Integer idPersona = resultSet.getInt("idPersona");
		String nombre = resultSet.getString("nombre");
		String apellido = resultSet.getString("apellido");
		Date fechaNac = resultSet.getDate("fechaNac");
		String telefono = resultSet.getString("telefono");
		String email = resultSet.getNString("email");
		Integer idDireccion = resultSet.getInt("idDireccion");
		Integer idTipoDeContacto = resultSet.getInt("idTipoDeContacto");
		PersonaDTO p =  new PersonaDTO(nombre, apellido, fechaNac, telefono, email, idDireccion, idTipoDeContacto);
		p.setIdPersona(idPersona);
		return p;
	}

}
