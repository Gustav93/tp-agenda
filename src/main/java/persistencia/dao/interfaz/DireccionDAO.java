package persistencia.dao.interfaz;

import java.util.List;

import dto.DireccionDTO;

public interface DireccionDAO {

	public boolean insert(DireccionDTO d);
	
	public boolean delete(DireccionDTO d);
	
	public boolean update(DireccionDTO d);
	
	public List<DireccionDTO> readAll();
	
	public DireccionDTO obtener(Integer id);
	
	public Integer maximoId();
	
}
