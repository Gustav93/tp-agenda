package persistencia.dao.interfaz;

import java.util.List;

import dto.PersonaDTO;

public interface PersonaDAO 
{
	
	public boolean insert(PersonaDTO p);

	public boolean delete(PersonaDTO p);
	
	public boolean update(PersonaDTO p);
	
	public List<PersonaDTO> readAll();
	
	public PersonaDTO obtener(Integer id);
}
