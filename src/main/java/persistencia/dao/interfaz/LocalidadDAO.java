package persistencia.dao.interfaz;

import java.util.List;

import dto.LocalidadDTO;

public interface LocalidadDAO {
	
	public boolean insert(LocalidadDTO l);

	public boolean delete(LocalidadDTO l);
	
	public boolean update(LocalidadDTO l);
	
	public List<LocalidadDTO> readAll();
	
	public LocalidadDTO obtener(Integer id);
	
	public LocalidadDTO obtenerID(String nombre);
}
