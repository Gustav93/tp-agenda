package persistencia.dao.interfaz;

import java.util.List;

import dto.TipoDeContactoDTO;

public interface TipoDeContactoDAO {

	public boolean insert(TipoDeContactoDTO t);

	public boolean delete(TipoDeContactoDTO t);
	
	public boolean update(TipoDeContactoDTO t);
	
	public List<TipoDeContactoDTO> readAll();
	
	public TipoDeContactoDTO obtener(Integer id);
	
	public TipoDeContactoDTO obtenerID(String nombre);
}
