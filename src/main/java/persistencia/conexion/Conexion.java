package persistencia.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.Logger;

import configuracion.propiedades.Propiedades;

public class Conexion 
{
	public static Conexion instancia;
	private Connection connection;
	private Logger log = Logger.getLogger(Conexion.class);	
	
	private Conexion()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver"); // quitar si no es necesario
			String[] datosDB = Propiedades.cargarConfiguracionDB();
			connection = DriverManager.getConnection("jdbc:mysql://"+datosDB[2]+":"+datosDB[3]+"/grupo_15", datosDB[0], datosDB[1]);
			connection.setAutoCommit(false);
			log.info("Conexión exitosa");
		}
		catch(Exception e)
		{
			return;
		}
	}
	
	
	public static Conexion getConexion()   
	{								
		if(instancia == null)
		{
			instancia = new Conexion();
		}
		return instancia;
	}

	public Connection getSQLConexion() 
	{
		return this.connection;
	}
	
	public void cerrarConexion()
	{
		try 
		{
			this.connection.close();
			log.info("Conexion cerrada");
		}
		catch (SQLException e) 
		{
			log.error("Error al cerrar la conexión!", e);
		}
		instancia = null;
	}
	
	public static boolean esValida()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			return false;
		}

		Connection connection = null;

		try 
		{
			//[0]: User, [1]: Password, [2]: IP, [3]:Puerto
			String[] datosDB = Propiedades.cargarConfiguracionDB();
			connection = DriverManager.getConnection("jdbc:mysql://"+datosDB[2]+":"+datosDB[3]+"/grupo_15", datosDB[0], datosDB[1]);

		} 
		catch (SQLException e) 
		{
			instancia = null;
			return false;
		}

		if (connection != null) {
			instancia = null;
			return true;
			
		}
		else {
			instancia = null;
			return false;
		}
	  }
}
