package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

import dto.LocalidadDTO;
import dto.TipoDeContactoDTO;

public class Verificador {
		
	public Verificador(){ }	

	public boolean validarCampos(String nombre, String apellido, String telefono, Date fecha, String email, String calle, String altura, String piso, String depto){
		String errores = "";
		
		if(!validarNombreApellido(nombre)){
			errores = errores + "Nombre; ";
		}
		if(!validarNombreApellido(apellido)) {
			errores = errores + "Apellido; ";
		}
		if(!validarTelefono(telefono)){
			errores = errores + "Telefono; ";
		}
		if(!validarFecha(fecha)){
			errores = errores + "Cumpleaños; ";
		}
		if(!validarEmail(email)){
			errores = errores + "Email; ";
		}
		if(!validarNombreCalle(calle)){
			errores = errores + "Calle; ";
		}
		if(!validarAltura(altura)){
			errores = errores + "Altura; ";
		}
		if(!validarPiso(piso)){
			errores = errores + "Piso; ";
		}
		if(!validarDepto(depto)){
			errores = errores + "Depto; ";
		}
		if(errores.length() > 0){
			JOptionPane.showMessageDialog(null,"Los siguientes campos tienen valor invalido o están vacios: " + errores);
			return false;
		}
		return true;
	}

	private boolean validarNombreApellido(String nombre){		
		String nombrePattern = "^[a-zA-Z ]*$";
		
		if(nombre.length() == 0){
			return false;
		}
		else if(Pattern.matches(nombrePattern, nombre)){
			return true;
		}
		else{
			return false;
		}
	}
		
	private boolean validarTelefono (String telefono) {
		String telefonoPattern = "\\d{4,6}" + "-" + "\\d{4}"; 
		
		if(telefono.length() == 0){
			return false;
		}
		else if(Pattern.matches(telefonoPattern, telefono)){
			return true;
		}
		else{
			return false;
		}	
	}
	
	private boolean validarFecha(Date fecha) {
        return fecha!=null;
    }
	
	private boolean FechaMayorA10000(String fecha) {
		String d, m, a;
		Integer posicion = fecha.indexOf("/");
		d = fecha.substring(0, posicion);
		fecha = fecha.substring(posicion + 1);
		posicion = fecha.indexOf("/");
		m = fecha.substring(0, posicion);
		fecha = fecha.substring(posicion + 1);
		a = fecha;
		return Integer.parseInt(a) >= 10000;
	}
	
	private boolean validarEmail(String email){
		String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" + "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";

		if(email.length() == 0){
			return false;
		}
		else if(Pattern.matches(emailPattern, email)){
				return true;
		}
		else{
			return false;
		}	
	}
		
	private boolean validarNombreCalle(String nombre){
		String nombrePattern = "^[a-zA-Z. ]*$";
		
		if(nombre.length() == 0){
			return false;
		}
		else if(Pattern.matches(nombrePattern, nombre)){
			return true;
		}
		else{
			return false;
		}	
	}
	
	private boolean validarAltura(String altura) {
		String alturaPattern = "\\d{3,5}";
			
		if(altura.length() == 0){
			return false;
		}
		else if(Pattern.matches(alturaPattern, altura)){
			return true;
		}
		else{
			return false;
		}		
	}
		
	private boolean validarPiso(String piso) {
		String pisoPattern = "\\d{1,2}";
		
		if(piso.length() == 0){
			return false;
		}
		else if(Pattern.matches(pisoPattern, piso)){
			return true;
		}
		else{
			return false;
		}	
	}
		
	private boolean validarDepto(String depto) {
		String deptoPattern = "\\d{1,2}";
		
		if(depto.length() == 0){
			return false;
		}
		else if(Pattern.matches(deptoPattern, depto)){
			return true;
		}
		else{
			return false;
		}	
	}
	
	public boolean validarTipoContacto(String nombre){
		String nombrePattern = "^[a-zA-Z. 0-9]*$";
		
		if(nombre.length() == 0){
			JOptionPane.showMessageDialog(null,"El tipo de contacto no puede ser vacio");
			return false;
		}
		else if(Pattern.matches(nombrePattern, nombre)){
			return true;
		}
		else{
			JOptionPane.showMessageDialog(null,"El tipo de contacto no es válido");
			return false;
		}	
	}
		
	public boolean validarLocalidad(String nombre){
		String nombrePattern = "^[0-9a-zA-Z. 0-9]*$";
		
		if(nombre.length() == 0){
			JOptionPane.showMessageDialog(null,"La localidad no puede ser vacia");
			return false;
		}
		else if(Pattern.matches(nombrePattern, nombre)){
			return true;
		}
		else{
			JOptionPane.showMessageDialog(null,"La localildad no es válida");
			return false;
		}	
	}
	
	public boolean validarBorradoCon(List<TipoDeContactoDTO> arr, String nombre){
		ArrayList<String> tipos = new ArrayList<String>();
	
		for(TipoDeContactoDTO tc: arr){
			tipos.add(tc.getNombre());
		}
		if(tipos.contains(nombre)){
			return false;
		}
		return true;
	}
		
	public boolean validarBorradoLoc(List<LocalidadDTO> arr, String nombre){
		ArrayList<String> tipos = new ArrayList<String>();
		
		for(LocalidadDTO loc: arr){
			tipos.add(loc.getNombre());
		}
		if(tipos.contains(nombre)){
			return false;
		}
		return true;
	}
//	
//	public boolean validarCamposInicio(String ip, String puerto, String usuario,String contraseÃ±a){
//		String errores = "";
//		
//		if(!validarIp(ip)){
//			errores = errores + "Ip; ";
//		}		
//		if(!validarPuerto(puerto)){
//			errores = errores + "Puerto; ";
//		}		
//		if(!validarUsuario(usuario)){
//			errores = errores + "Usuario; ";
//		}
//		if(!validarContraseÃ±a(contraseÃ±a)){
//			errores = errores + "ContraseÃ±a; ";
//		}
//		if(errores.length() > 0){
//			JOptionPane.showMessageDialog(null,"Los siguientes campos tienen valor invalido o estÃ¡n vacios: " + errores);
//			return false;
//		}		
//		return true;
//	}
//	
//	private boolean validarIp(String ip){
//		String nombrePattern = "^[0-9a-zA-Z(\\.)(\\:))(\\/)(\\_)]*$";
//		
//		if(ip.length() == 0){
//			return false;
//		}
//		else if(Pattern.matches(nombrePattern, ip)){
//			return true;
//		}
//		else{
//			return false;
//		}	
//	}
//	
//	private boolean validarPuerto(String ip){
//		String nombrePattern = "\\d{4}";
//		
//		if(ip.length() == 0){
//			return false;
//		}
//		else if(Pattern.matches(nombrePattern, ip)){
//			return true;
//		}
//		else{
//			return false;
//		}	
//	}
//
//	private boolean validarUsuario(String ip){
//		String nombrePattern = "^[0-9a-zA-Z(\\_)]*$";
//		
//		if(ip.length() == 0){
//			return false;
//		}
//		else if(Pattern.matches(nombrePattern, ip)){
//			return true;
//		}
//		else{
//			return false;
//		}	
//	}
//	
//	private boolean validarContraseÃ±a(String ip){
//		String nombrePattern = "^[0-9a-zA-Z]*$";
//		
//		if(ip.length() == 0){
//			return false;
//		}
//		else if(Pattern.matches(nombrePattern, ip)){
//			return true;
//		}
//		else{
//			return false;
//		}	
//	}
//		
//	public boolean autorizarConexion(String ip, String puerto, String usuario, String contraseÃ±a) {
//		if(!ip.equals("localhost") || !puerto.equals("3306") || !usuario.equals("root") || !contraseÃ±a.equals("pass")) {
//			return false;
//		}		
//		return true;
//	}
}
