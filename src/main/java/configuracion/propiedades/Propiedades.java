package configuracion.propiedades;
 
import java.io.IOException;
import java.util.Properties;


import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class Propiedades 
{
	public static Propiedades instancia;
	private String usuario;
	private String password;
	private String ip;
	private String puerto;
	
	private Propiedades() 
	{
	}
	
	public static void guardarConfiguracionDB(String usuario, String password, String ip, String puerto)
	{
		Properties prop = new Properties();
		OutputStream output = null;
		
		try 
		{
			output = new FileOutputStream("config.properties");
			
			prop.setProperty("dbUser", usuario);
			prop.setProperty("dbPassword", password);
			prop.setProperty("dbIp", ip);
			prop.setProperty("dbPuerto", puerto);

			prop.store(output, null);
			
		}
		catch (IOException io) 
		{
			io.printStackTrace();
		}
		finally 
		{
			if (output != null) 
			{
				try 
				{
					output.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String[] cargarConfiguracionDB()
	{
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("config.properties");

			prop.load(input);

			
			return new String[] {prop.getProperty("dbUser"), prop.getProperty("dbPassword"), prop.getProperty("dbIp"), prop.getProperty("dbPuerto")};

		} 
		catch (IOException ex) 
		{
			ex.printStackTrace();
		} finally 
		{
			if (input != null) 
			{
				try 
				{
					input.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static Propiedades getPropiedades()   	
	{							
		if(instancia == null)
		{
			instancia = new Propiedades();
		}
		return instancia;
	}

	public String getUsuario() 
	{
		return this.usuario;
	}

	public String getPassword() {
		return password;
	}

	public String getIp() {
		return ip;
	}

	public String getPuerto() {
		return puerto;
	}
	
}

