package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.swing.JOptionPane;

import configuracion.propiedades.Propiedades;
import modelo.Agenda;
import persistencia.conexion.Conexion;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaAgregarModificar;
import presentacion.vista.VentanaConfiguraciónConexion;
import presentacion.vista.VentanaOpcionesABM;
import presentacion.vista.VentanaPersona;
import presentacion.vista.Vista;
import utils.Verificador;
import dto.DireccionDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.ReportePersonaDTO;
import dto.TipoDeContactoDTO;

public class Controlador implements ActionListener {
	private Vista vista;
	private List<PersonaDTO> personasEnTabla;
	private VentanaPersona ventanaPersona;
	private VentanaOpcionesABM ventanaOpcionLocalidad;
	private VentanaAgregarModificar ventanaLocalidad;
	private VentanaOpcionesABM ventanaOpcionTipoDeContacto;
	private VentanaAgregarModificar ventanaTipoDeContacto;
	private Verificador verificador;
	
	private VentanaConfiguraciónConexion ventanaConfiguracionConexion;
	private boolean respuestaVentanaConfiguracion;
	
	private Agenda agenda;
	private final String DATE_FORMAT = "dd-MM-YYYY";

	public Controlador(Vista vista, Agenda agenda) {
		
		this.vista = vista;
		this.vista.getBtnAgregar().addActionListener(a -> ventanaAgregarPersona(a));
		this.vista.getBtnEditar().addActionListener(b -> ventanaEditarPersona(b));
		this.vista.getBtnBorrar().addActionListener(s -> borrarPersona(s));
		this.vista.getBtnReporte().addActionListener(r -> mostrarReporte(r));
		this.vista.getBtnConfig().addActionListener(t -> cargarVentanaConexion(t));
		this.ventanaPersona = VentanaPersona.getInstance();
		this.ventanaPersona.getBtnAgregarPersona().addActionListener(p -> guardarPersona(p));

		this.vista.getBtnABMtipoDeContacto().addActionListener(c -> ventanaAMBtipoDeContacto(c));
		this.vista.getBtnABMLocalidad().addActionListener(d -> ventanaABMlocalidad(d));
		this.verificador = new Verificador();
		this.agenda = agenda;
	}
	
	

	// Conexion
	private void confimarConexion(ActionEvent e)
	{
		Propiedades.guardarConfiguracionDB(this.ventanaConfiguracionConexion.getTxtUsuario().getText(),
				this.ventanaConfiguracionConexion.getTxtPassword().getText(), this.ventanaConfiguracionConexion.getTxtIp().getText(), this.ventanaConfiguracionConexion.getTxtPuerto().getText());
	
		if(Conexion.esValida()) {
			this.respuestaVentanaConfiguracion = true;
			this.ventanaConfiguracionConexion.dispose();
		}
		else{
			JOptionPane.showMessageDialog(null, "Error al conectarse con la base de datos, verifique datos.");
			return;
		}
	}
	
	private void salirConexion(ActionEvent e)
	{
		if(!this.vista.getVisible())
			System.exit(0);
		else
		{
			this.ventanaConfiguracionConexion.setVisible(false);
			this.ventanaConfiguracionConexion.getTxtUsuario().setText(Propiedades.cargarConfiguracionDB()[0]);
			this.ventanaConfiguracionConexion.getTxtPassword().setText(Propiedades.cargarConfiguracionDB()[1]);
			this.ventanaConfiguracionConexion.getTxtIp().setText(Propiedades.cargarConfiguracionDB()[2]);
			this.ventanaConfiguracionConexion.getTxtPuerto().setText(Propiedades.cargarConfiguracionDB()[3]);
			this.confimarConexion(e);
		}
			
	}
	
	private void esperandoRespuesta()
	{
		while(!Conexion.esValida()) 
		{
			 if(this.ventanaConfiguracionConexion == null) {
				 this.ventanaConfiguracionConexion = new VentanaConfiguraciónConexion(this);
				 this.ventanaConfiguracionConexion.getBtnConfirmar().addActionListener(e -> this.confimarConexion(e));
				 this.ventanaConfiguracionConexion.getBtnSalir().addActionListener(e -> this.salirConexion(e));
				 this.respuestaVentanaConfiguracion = false;
			 }
				
			 while(respuestaVentanaConfiguracion == false)
			 { 
//				 System.out.println("Esperando respuesta del usuario...");
				 try
				 {
					Thread.sleep(5000);
				 } 
				 catch (InterruptedException e) 
				 {
					e.printStackTrace();
				 }
			 }
		}
	}
	
	private void cargarVentanaConexion(ActionEvent t) {
		this.ventanaConfiguracionConexion = new VentanaConfiguraciónConexion(this);
		this.ventanaConfiguracionConexion.getTxtUsuario().setText(Propiedades.cargarConfiguracionDB()[0]);
		this.ventanaConfiguracionConexion.getTxtPassword().setText(Propiedades.cargarConfiguracionDB()[1]);
		this.ventanaConfiguracionConexion.getTxtIp().setText(Propiedades.cargarConfiguracionDB()[2]);
		this.ventanaConfiguracionConexion.getTxtPuerto().setText(Propiedades.cargarConfiguracionDB()[3]);
		this.ventanaConfiguracionConexion.getBtnConfirmar().addActionListener(e -> this.confimarConexion(e));
		this.ventanaConfiguracionConexion.getBtnSalir().addActionListener(e -> this.salirConexion(e));
	}
	
	//Localidad

	private void ventanaABMlocalidad(ActionEvent d) {
		this.ventanaOpcionLocalidad = new VentanaOpcionesABM(this);
		this.llenarTablaLocalidades();
		this.ventanaOpcionLocalidad.getBtnAgregar().addActionListener(a -> ventanaAgregarLocalidad(a));
		this.ventanaOpcionLocalidad.getBtnBorrar().addActionListener(b -> ventanaBorrarLocalidad(b));
		this.ventanaOpcionLocalidad.getBtnEditar().addActionListener(c -> VentanaEditarLocalidad(c));
	}

	private void VentanaEditarLocalidad(ActionEvent c) {

		LocalidadDTO localidad = null;
		int[] filas_seleccionadas = this.ventanaOpcionLocalidad.getTablaOpciones().getSelectedRows();
		if (filas_seleccionadas.length != 1) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar una Localidad para poder editar");
		} else {
			this.ventanaLocalidad = new VentanaAgregarModificar(this, "Modificar Localidad");
			localidad = this.obtenerLocalidadEnTabla();
			this.ventanaLocalidad.setId(localidad.getIdLocalidad());
			this.ventanaLocalidad.getNombre().setText(localidad.getNombre());
			this.ventanaLocalidad.getBtnConfirmar().addActionListener(b -> guardarLocalidad());
		}
	}

	private void ventanaAgregarLocalidad(ActionEvent a) {
		this.ventanaLocalidad = new VentanaAgregarModificar(this, "Agregar Localidad");
		this.ventanaLocalidad.getBtnConfirmar().addActionListener(b -> guardarLocalidad());
	}

	private void guardarLocalidad() {
		Integer idLocalidad = this.ventanaLocalidad.getId();
		if (this.verificador.validarLocalidad(this.ventanaLocalidad.getNombre().getText())) {
			if (idLocalidad == null) {
				String nombre = this.ventanaLocalidad.getNombre().getText();
				this.agenda.agregarLocalidad(new LocalidadDTO(nombre));
				this.llenarTablaLocalidades();
				this.ventanaLocalidad.limpiarVentana();
				;
			} else {
				String nombre = this.ventanaLocalidad.getNombre().getText();
				LocalidadDTO l = new LocalidadDTO(nombre);
				l.setIdLocalidad(idLocalidad);
				this.agenda.editarLocalidad(l);
				this.llenarTablaLocalidades();
				this.ventanaLocalidad.limpiarVentana();
			}
			this.llenarTabla(this.personasEnTabla);
		}

	}

	private void ventanaBorrarLocalidad(ActionEvent a) {
		int[] filasSeleccionadas = this.ventanaOpcionLocalidad.getTablaOpciones().getSelectedRows();
		if(filasSeleccionadas.length > 0) {
			int confirm = JOptionPane.showOptionDialog(null,
					"¿Estás seguro que quieres borrar el o las Localidades seleccionados?", "Confirmación",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (confirm == 0) {
				List<LocalidadDTO> ls = this.agenda.obtenerLocalidades();
				
				for (int fila : filasSeleccionadas) {
					if (this.contactoAsiociado(ls.get(fila))) {
						JOptionPane.showMessageDialog(null,
								"No se pudo Eliminar: " + ls.get(fila).getNombre() + ", Esta asociada a una Contacto");
					} else
						this.agenda.borrarLocalidad(ls.get(fila));
				}
				this.llenarTablaLocalidades();
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Debe seleccionar al menos una Localidad para poder borrar");
		}
		
	}

	private LocalidadDTO obtenerLocalidadEnTabla() {
		int filaSeleccionada = this.ventanaOpcionLocalidad.getTablaOpciones().getSelectedRow();
		return this.agenda.obtenerLocalidades().get(filaSeleccionada);
	}

	private void ventanaAMBtipoDeContacto(ActionEvent c) {
		this.ventanaOpcionTipoDeContacto = new VentanaOpcionesABM(this);
		this.llenarTablaTipoDeContactos();
		this.ventanaOpcionTipoDeContacto.getBtnAgregar().addActionListener(x -> ventanaAgregarTipoDeContacto(x));
		this.ventanaOpcionTipoDeContacto.getBtnBorrar().addActionListener(y -> ventanaBorrarTipoDeContacto(y));
		this.ventanaOpcionTipoDeContacto.getBtnEditar().addActionListener(z -> ventanaEditarTipoDeContacto(z));
	}

	private void ventanaEditarTipoDeContacto(ActionEvent z) {
		TipoDeContactoDTO tipo = null;
		int[] filas_seleccionadas = this.ventanaOpcionTipoDeContacto.getTablaOpciones().getSelectedRows();
		if (filas_seleccionadas.length != 1) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un Tipo de Contacto para poder editar");
		} else {
			this.ventanaTipoDeContacto = new VentanaAgregarModificar(this, "Modificar Modificar Tipo de Contacto");
			tipo = this.obtenerTipoDeContactoEnTabla();
			this.ventanaTipoDeContacto.setId(tipo.getIdTipoDeContacto());
			this.ventanaTipoDeContacto.getNombre().setText(tipo.getNombre());
			this.ventanaTipoDeContacto.getBtnConfirmar().addActionListener(b -> guardarTipoDeContacto());
		}
		
	}

	private void ventanaBorrarTipoDeContacto(ActionEvent y) {
		int[] filasSeleccionadas = this.ventanaOpcionLocalidad.getTablaOpciones().getSelectedRows();
		if(filasSeleccionadas.length > 0) {
			int confirm = JOptionPane.showOptionDialog(null,
					"¿Estás seguro que quieres borrar el o los Tipos de Contactos seleccionados?", "Confirmación",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (confirm == 0) {
				List<TipoDeContactoDTO> tipos = this.agenda.obteneTipoDeContactos();
				for (int fila : filasSeleccionadas) {
					if (this.contactoAsiociado(tipos.get(fila))) {
						JOptionPane.showMessageDialog(null, "No se pudo Eliminar: " + tipos.get(fila).getNombre()
								+ ", /n Esta asociado a una Contacto");
					} else
						this.agenda.borrarTipoDeContacto(tipos.get(fila));
				}
				this.llenarTablaTipoDeContactos();
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Debe seleccionar al menos una Tipo de Contacto para poder borrar");
		}
		
	}

	private void ventanaAgregarTipoDeContacto(ActionEvent x) {
		this.ventanaTipoDeContacto = new VentanaAgregarModificar(this, "Agregar Tipo de Contacto");
		this.ventanaTipoDeContacto.getBtnConfirmar().addActionListener(b -> guardarTipoDeContacto());
	}

	private void guardarTipoDeContacto() {
		Integer idTipoDeContacto = this.ventanaTipoDeContacto.getId();
		if (this.verificador.validarTipoContacto(this.ventanaTipoDeContacto.getNombre().getText())) {
			if (idTipoDeContacto == null) {
				String nombre = this.ventanaTipoDeContacto.getNombre().getText();
				this.agenda.agregarTipoDeContacto(new TipoDeContactoDTO(nombre));
				this.llenarTablaTipoDeContactos();
				this.ventanaTipoDeContacto.limpiarVentana();
			} else {
				String nombre = this.ventanaTipoDeContacto.getNombre().getText();
				TipoDeContactoDTO t = new TipoDeContactoDTO(nombre);
				t.setIdTipoDeContacto(idTipoDeContacto);
				this.agenda.editarTipoDeContacto(t);
				this.llenarTablaTipoDeContactos();
				this.ventanaTipoDeContacto.limpiarVentana();
			}
		}

		this.llenarTabla(this.personasEnTabla);
	}

	private void llenarTablaTipoDeContactos() {
		this.ventanaOpcionTipoDeContacto.getModelOpciones().setRowCount(0);
		this.ventanaOpcionTipoDeContacto.getModelOpciones().setColumnCount(0);
		this.ventanaOpcionTipoDeContacto.getModelOpciones().setColumnIdentifiers(this.ventanaOpcionTipoDeContacto.getNombreColumnas());
		for (TipoDeContactoDTO l : this.agenda.obteneTipoDeContactos()) {
			Object[] fila = { l.getNombre() };
			this.ventanaOpcionTipoDeContacto.getModelOpciones().addRow(fila);
		}
	}

	private TipoDeContactoDTO obtenerTipoDeContactoEnTabla() {
		int filaSeleccionada = this.ventanaOpcionTipoDeContacto.getTablaOpciones().getSelectedRow();
		return this.agenda.obteneTipoDeContactos().get(filaSeleccionada);
	}

	private void ventanaAgregarPersona(ActionEvent a) {
		this.ventanaPersona.limpiarVentana();
		this.ventanaPersona.llenarCombos(agenda);
		this.ventanaPersona.mostrarVentana();
	}

	private void guardarPersona(ActionEvent p) {

		Integer idPersona = this.ventanaPersona.getIdPersona();
		Integer idDireccion = this.ventanaPersona.getIdDireccion();

		String nombre = this.ventanaPersona.getTxtNombre().getText();
		String apellido = this.ventanaPersona.getTxtApellido().getText();
		Date cumple = this.ventanaPersona.getDateChooserCumple().getDate();
		String tel = this.ventanaPersona.getTxtTelefono().getText();
		String email = this.ventanaPersona.getTxtEmail().getText();
		String tipo = this.ventanaPersona.getTipoDeContactoSeleccionado();
		Integer idTipo = this.agenda.obtenerIdTipoDeContacto(tipo).getIdTipoDeContacto();
		String calle = this.ventanaPersona.getTxtCalle().getText();
		String altura = this.ventanaPersona.getTxtAltura().getText();
		String piso = this.ventanaPersona.getTxtPiso().getText();
		String depto = this.ventanaPersona.getTxtDepto().getText();
		String localidad = this.ventanaPersona.getLocalidadSeleccionada();
		Integer idLocalidad = this.agenda.obtenerIdLocalidad(localidad).getIdLocalidad();
		DireccionDTO direccion = new DireccionDTO(calle, altura, piso, depto, idLocalidad);
		PersonaDTO persona;

		if(this.verificador.validarCampos(nombre, apellido, tel, cumple, email, calle, altura, piso, depto))
		{
			long d = this.ventanaPersona.getDateChooserCumple().getDate().getTime();
			if (idPersona == null) {
				this.agenda.agregarDireccion(direccion);
				Integer idDir = this.agenda.obtenerMaximoId();
				persona = new PersonaDTO(nombre, apellido, new java.sql.Date(d), tel, email, idDir, idTipo);
				this.agenda.agregarPersona(persona);
			} else {
				direccion.setIdDireccion(idDireccion);
				this.agenda.editarDireccion(direccion);
				persona = new PersonaDTO(nombre, apellido, new java.sql.Date(d), tel, email, idDireccion, idTipo);
				persona.setIdPersona(idPersona);
				this.agenda.editarPersona(persona);
			}
			
			this.refrescarTabla();
			this.ventanaPersona.cerrar();
		}	
	}

	private void ventanaEditarPersona(ActionEvent b) {
		PersonaDTO persona = null;
		int[] filas_seleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		if (filas_seleccionadas.length != 1) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un contacto para poder editar");
		} else {
			this.ventanaPersona.llenarCombos(agenda);
			this.ventanaPersona.mostrarVentana();
			persona = this.obtenerPersonaEntabla();
			this.ventanaPersona.setIdPersona(persona.getIdPersona());
			this.ventanaPersona.getTxtNombre().setText(persona.getNombre());
			this.ventanaPersona.getTxtApellido().setText(persona.getApellido());
			this.extraerFecha(persona);
			this.ventanaPersona.getTxtTelefono().setText(persona.getTelefono());
			this.ventanaPersona.getTxtEmail().setText(persona.getEmail());
			int idTipo = persona.getIdTipoContacto();
			TipoDeContactoDTO tipo = this.agenda.obtenerDatosTipoDeContacto(idTipo);
			this.ventanaPersona.getTipoDeContacto().setSelectedItem(tipo.getNombre());
			int idDir = persona.getIdDireccion();
			this.ventanaPersona.setIdDireccion(idDir);
			DireccionDTO dir = this.agenda.obtenerDatosDireccion(idDir);
			this.ventanaPersona.getTxtCalle().setText(dir.getCalle());
			this.ventanaPersona.getTxtAltura().setText(dir.getAltura());
			this.ventanaPersona.getTxtPiso().setText(dir.getPiso());
			this.ventanaPersona.getTxtDepto().setText(dir.getDepto());
			int idLoc = dir.getIdLocalidad();
			LocalidadDTO loc = this.agenda.obtenerDatosLocalidad(idLoc);
			this.ventanaPersona.getLocalidad().setSelectedItem(loc.getNombre());
		}
	}

	private void extraerFecha(PersonaDTO persona) {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date d = f.parse(persona.getFechaNac().toString());
			this.ventanaPersona.setDateChooserCumple(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private void mostrarReporte(ActionEvent r) {
		ReporteAgenda reporte = new ReporteAgenda(this.getReportePersonas());
		reporte.mostrar();
	}

	public void borrarPersona(ActionEvent s) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas().getSelectedRows();
		if (filasSeleccionadas.length > 0) {
			int confirm = JOptionPane.showOptionDialog(null,
					"¿Estás seguro que quieres borrar el o los contactos seleccionados?", "Confirmación",
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (confirm == 0) {
				for (int fila : filasSeleccionadas) {
					Integer idDir = this.personasEnTabla.get(fila).getIdDireccion();
					DireccionDTO dir = this.agenda.obtenerDatosDireccion(idDir);
					this.agenda.borrarPersona(this.personasEnTabla.get(fila));
					if (!this.contactoAsociado(dir))
						this.agenda.borrarDireccion(dir);
				}

				this.refrescarTabla();
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Debe seleccionar al menos una persona para poder borrar");
		}
	}

	public void inicializar() {
		this.esperandoRespuesta();
		this.refrescarTabla();
		this.vista.show();
	}

	private void refrescarTabla() {
		this.personasEnTabla = agenda.obtenerPersonas();
		this.llenarTabla(this.personasEnTabla);
	}

	private void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.vista.getModelPersonas().setRowCount(0); // Para vaciar la tabla
		this.vista.getModelPersonas().setColumnCount(0);
		this.vista.getModelPersonas().setColumnIdentifiers(this.vista.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla) {
			String nombre = p.getNombre();
			String apellido = p.getApellido();
			String fechaNac = this.convertirFecha(p.getFechaNac().toString());
			
			String tel = p.getTelefono();
			String email = p.getEmail();
			TipoDeContactoDTO t = obtenerTipoDeContactoRelacionado(p.getIdTipoContacto());
			DireccionDTO d = obtenerDireccionRelacionada(p.getIdDireccion());
			LocalidadDTO l = obtenerLocalidadRelacionada(d.getIdLocalidad());
			Object[] fila = { nombre, apellido, fechaNac, tel,
					email, t.getNombre(), d.getCalle(), d.getAltura(), d.getPiso(), d.getDepto(), l.getNombre() };
			this.vista.getModelPersonas().addRow(fila);
		}
	}

	private LocalidadDTO obtenerLocalidadRelacionada(Integer idLocalidad) {
		return this.agenda.obtenerDatosLocalidad(idLocalidad);
	}

	private DireccionDTO obtenerDireccionRelacionada(Integer idDireccion) {
		return this.agenda.obtenerDatosDireccion(idDireccion);
	}

	private TipoDeContactoDTO obtenerTipoDeContactoRelacionado(Integer idTipoContacto) {
		return this.agenda.obtenerDatosTipoDeContacto(idTipoContacto);
	}

	private PersonaDTO obtenerPersonaEntabla() {
		int filaSeleccionada = this.vista.getTablaPersonas().getSelectedRow();
		return personasEnTabla.get(filaSeleccionada);
	}

	private void llenarTablaLocalidades() {
		this.ventanaOpcionLocalidad.getModelOpciones().setRowCount(0);
		this.ventanaOpcionLocalidad.getModelOpciones().setColumnCount(0);
		this.ventanaOpcionLocalidad.getModelOpciones().setColumnIdentifiers(this.ventanaOpcionLocalidad.getNombreColumnas());
		for (LocalidadDTO l : this.agenda.obtenerLocalidades()) {
			Object[] fila = { l.getNombre() };
			this.ventanaOpcionLocalidad.getModelOpciones().addRow(fila);
		}
	}

	private String convertirFecha(String fecha) {
		String d, m, a;
		Integer posicion = fecha.indexOf("-");
		a = fecha.substring(0, posicion);
		fecha = fecha.substring(posicion + 1);
		posicion = fecha.indexOf("-");
		m = fecha.substring(0, posicion);
		fecha = fecha.substring(posicion + 1);
		d = fecha;
		return d + "/" + m + "/" + a;
	}

	private boolean contactoAsiociado(TipoDeContactoDTO t) {
		for (PersonaDTO p : personasEnTabla) {
			if (p.getIdTipoContacto() == t.getIdTipoDeContacto())
				return true;
		}
		return false;
	}

	private boolean contactoAsiociado(LocalidadDTO l) {
		for (DireccionDTO d : this.agenda.obteneDirecciones()) {
			if (d.getIdLocalidad() == l.getIdLocalidad())
				return true;
		}
		return false;
	}

	private boolean contactoAsociado(DireccionDTO d) {
		for (PersonaDTO p : personasEnTabla) {
			if (p.getIdDireccion() == d.getIdDireccion())
				return true;
		}
		return false;
	}

	private List<ReportePersonaDTO> getReportePersonas() {
		List<ReportePersonaDTO> res = new ArrayList<>();
		this.agenda.obtenerPersonas().forEach(p->{
			ReportePersonaDTO persona = new ReportePersonaDTO();
			DireccionDTO direccion = this.agenda.obtenerDatosDireccion(p.getIdDireccion());
			LocalidadDTO localidad = this.agenda.obtenerDatosLocalidad(direccion.getIdLocalidad()); 
			TipoDeContactoDTO tipoContacto = this.agenda.obtenerDatosTipoDeContacto(p.getIdTipoContacto());
			persona.setNombre(p.getNombre() != null ? p.getNombre() : "");
			persona.setApellido(p.getApellido() != null ? p.getApellido() : "");
			persona.setTelefono(p.getTelefono() != null ? p.getTelefono() : "");
			persona.setCalle((direccion != null && direccion.getCalle()!=null)?direccion.getCalle() : "");
			persona.setAltura((direccion != null && direccion.getAltura() != null) ? direccion.getAltura() : "");
			persona.setPiso((direccion != null && direccion.getPiso() != null) ? direccion.getPiso() : "");
			persona.setLocalidad((localidad != null && localidad.getNombre()!= null) ? localidad.getNombre( ): "");
			persona.setFechaNacimiento(p.getFechaNac() != null ? new SimpleDateFormat(DATE_FORMAT).format(p.getFechaNac()) : "");
			persona.setTipoContacto((tipoContacto != null&&tipoContacto.getNombre() != null) ? tipoContacto.getNombre() : "");
			persona.setEmail(p.getEmail() != null ? p.getEmail() : "");
			persona.setTotales(this.agenda.obtenerPersonas().size());
			res.add(persona);
		});
		return agrupar(res);
	}
	
	private List<ReportePersonaDTO> agrupar(List<ReportePersonaDTO> reportePersonaDTOList)
	{
		List<ReportePersonaDTO> personas = new ArrayList<ReportePersonaDTO>(reportePersonaDTOList);
		List<ReportePersonaDTO> res = new ArrayList<ReportePersonaDTO>();
		HashMap <Integer,List <ReportePersonaDTO>> datosAgrupados = new HashMap<Integer, List<ReportePersonaDTO>>() ;
		
		for (ReportePersonaDTO reportePersonaDTO : personas) {
			if(!datosAgrupados.containsKey(reportePersonaDTO.intMes())) {
				datosAgrupados.put(reportePersonaDTO.intMes(), new ArrayList<>());
			}
			datosAgrupados.get(reportePersonaDTO.intMes()).add(reportePersonaDTO);
		}

		for (Integer clave : datosAgrupados.keySet()) {
			Collections.sort((datosAgrupados.get(clave)));
			res.addAll(datosAgrupados.get(clave));
		}
		return res;
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {

	}
}
