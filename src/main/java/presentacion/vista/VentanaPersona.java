package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import dto.LocalidadDTO;
import dto.TipoDeContactoDTO;
import modelo.Agenda;

import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;
import java.awt.Color;

public class VentanaPersona extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JButton btnAgregarPersona;
	private static VentanaPersona INSTANCE;
	private JTextField txtApellido;
	private JLabel lblEmail;
	private JComboBox<String> comboBoxTipoDeContacto;
	private JTextField txtEmail;
	private JLabel lblTipoDeContacto;
	private JLabel lblCalle;
	private JTextField txtCalle;
	private JLabel lblAltura;
	private JTextField txtAltura;
	private JLabel lblPiso;
	private JTextField txtPiso;
	private JLabel lblDepto;
	private JTextField txtDepto;
	private JLabel lblLocalidad;
	private JComboBox<String> comboBoxLocalidad;
	JDateChooser dateChooserCumple;
	private Integer idPersona = null;
	private Integer idDireccion = null;

	public Integer getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Integer id) {
		this.idPersona = id;
	}

	
	public Integer getIdDireccion() {
		return idDireccion;
	}

	public void setIdDireccion(Integer id) {
		this.idDireccion = id;
	}

	public static VentanaPersona getInstance()
	{
		
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaPersona(); 	
			return new VentanaPersona();
		}
		else
			return INSTANCE;
	}

	private VentanaPersona() 
	{
		super();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 343, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 315, 348);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 14, 113, 14);
		panel.add(lblNombre);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 94, 113, 14);
		panel.add(lblTelfono);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(133, 8, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(133, 91, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		btnAgregarPersona = new JButton("Confirmar");
		btnAgregarPersona.setBounds(198, 314, 107, 23);
		panel.add(btnAgregarPersona);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(10, 42, 113, 14);
		panel.add(lblApellido);
		
		txtApellido = new JTextField();
		txtApellido.setBounds(133, 39, 164, 20);
		panel.add(txtApellido);
		txtApellido.setColumns(10);
		
		JLabel lblCumple = new JLabel("Cumpleaños");
		lblCumple.setBounds(10, 69, 113, 14);
		panel.add(lblCumple);
		
		lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 119, 113, 14);
		panel.add(lblEmail);
		
		txtEmail = new JTextField("ejemplo@mail.com");
		txtEmail.setBounds(133, 116, 164, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		lblTipoDeContacto = new JLabel("Tipo De Contacto");
		lblTipoDeContacto.setBounds(10, 144, 113, 14);
		panel.add(lblTipoDeContacto);
		
		comboBoxTipoDeContacto = new JComboBox<>();
		comboBoxTipoDeContacto.setBounds(133, 141, 164, 20);
		panel.add(comboBoxTipoDeContacto);
		
		lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 169, 113, 14);
		panel.add(lblCalle);
		
		txtCalle = new JTextField();
		txtCalle.setBounds(133, 166, 164, 20);
		panel.add(txtCalle);
		txtCalle.setColumns(10);
		
		lblAltura = new JLabel("Altura");
		lblAltura.setBounds(10, 194, 113, 14);
		panel.add(lblAltura);
		
		txtAltura = new JTextField();
		txtAltura.setBounds(133, 191, 164, 20);
		panel.add(txtAltura);
		txtAltura.setColumns(10);
		
		lblPiso = new JLabel("Piso");
		lblPiso.setBounds(10, 219, 113, 14);
		panel.add(lblPiso);
		
		txtPiso = new JTextField();
		txtPiso.setBounds(133, 216, 164, 20);
		panel.add(txtPiso);
		txtPiso.setColumns(10);
		
		lblDepto = new JLabel("Depto");
		lblDepto.setBounds(10, 244, 113, 14);
		panel.add(lblDepto);
		
		txtDepto = new JTextField();
		txtDepto.setBounds(132, 241, 165, 20);
		panel.add(txtDepto);
		txtDepto.setColumns(10);
		
		lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 269, 113, 14);
		panel.add(lblLocalidad);
		
		comboBoxLocalidad = new JComboBox<>();
		comboBoxLocalidad.setBounds(133, 266, 164, 20);
		panel.add(comboBoxLocalidad);
		
		dateChooserCumple = new JDateChooser();
		dateChooserCumple.getCalendarButton().setBackground(Color.RED);
		dateChooserCumple.getDateEditor().setEnabled(false);
		this.dateChooserCumple.setDate(new java.util.Date());
		dateChooserCumple.setBounds(133, 63, 164, 20);
		panel.add(dateChooserCumple);
		
		this.setVisible(false);
	}

	public JTextField getTxtApellido() {
		return txtApellido;
	}

	public JDateChooser getDateChooserCumple() {
		return dateChooserCumple;
	}

	public void setDateChooserCumple(java.util.Date cumple) {
		System.out.println(cumple.toString());
		this.dateChooserCumple.setDate(cumple);
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public JTextField getTxtCalle() {
		return txtCalle;
	}

	public JTextField getTxtAltura() {
		return txtAltura;
	}

	public JTextField getTxtPiso() {
		return txtPiso;
	}

	public JTextField getTxtDepto() {
		return txtDepto;
	}

	public void mostrarVentana()
	{
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}

	public JButton getBtnAgregarPersona() 
	{
		return btnAgregarPersona;
	}
	
	public String getLocalidadSeleccionada() 
	{
		return this.comboBoxLocalidad.getSelectedItem().toString();
	}
	
	public String getTipoDeContactoSeleccionado()
	{
		return this.comboBoxTipoDeContacto.getSelectedItem().toString();
	}

	public void limpiarVentana()
	{
		this.idPersona = null;
		this.idDireccion = null;
		this.txtNombre.setText(null);
		this.txtApellido.setText(null);
//		this.txtCumple.setText("dd/mm/aaaa");
		this.dateChooserCumple.setDate(null);
		this.txtTelefono.setText(null);
		this.txtEmail.setText("ejemplo@mail.com");
		this.txtCalle.setText(null);
		this.txtAltura.setText(null);
		this.txtPiso.setText(null);
		this.txtDepto.setText(null);
	}
	public void cerrar()
	{
		this.limpiarVentana();
		this.dispose();
	}

	public void llenarCombos(Agenda a) {
		this.comboBoxTipoDeContacto.removeAllItems();
		for (TipoDeContactoDTO con : a.obteneTipoDeContactos()) {
			this.comboBoxTipoDeContacto.addItem(con.getNombre());
		}
		
		this.comboBoxLocalidad.removeAllItems();
		for (LocalidadDTO l : a.obtenerLocalidades()) {
			this.comboBoxLocalidad.addItem(l.getNombre());
		}
		
	}
	
	public JComboBox<String> getTipoDeContacto() {
		return this.comboBoxTipoDeContacto;
	}
	
	public JComboBox<String> getLocalidad() {
		return this.comboBoxLocalidad;
	}
}

