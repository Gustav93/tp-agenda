package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import presentacion.controlador.Controlador;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

public class VentanaAgregarModificar extends JFrame{
		
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Controlador controlador;
	private JTextField nombre;
	private JButton btnConfirmar;
	private Integer id = null;
	
	public VentanaAgregarModificar(Controlador controlador, String msj){
		
		super();
		this.controlador = controlador;
	
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 343, 183);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 307, 123);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblIngreseLaLocalidad = new JLabel(msj);
		lblIngreseLaLocalidad.setBounds(71, 11, 171, 14);
		panel.add(lblIngreseLaLocalidad);
		
		nombre = new JTextField();
		nombre.setBounds(71, 48, 171, 20);
		panel.add(nombre);
		nombre.setColumns(10);
				
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addActionListener(this.controlador);
		btnConfirmar.setBounds(111, 89, 89, 23);
		panel.add(btnConfirmar);

		this.setVisible(true);
}
	
	public void limpiarVentana(){
		this.id = null;
		this.getNombre().setText("");
		this.setVisible(false);
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer i) {
		this.id = i;
	}

	public JButton getBtnConfirmar() {
		return btnConfirmar;
	}

	public JTextField getNombre() {
		return nombre;
	}
}
