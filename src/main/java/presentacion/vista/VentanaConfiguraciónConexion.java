package presentacion.vista;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import presentacion.controlador.Controlador;

public class VentanaConfiguraciónConexion extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtUsuario;
	private JTextField txtPassword;
	private JTextField txtIp;
	private JTextField txtPuerto;

	private JButton btnConfirmarConfiguracionConexion;
	private JButton btnSalir;
	
	private Controlador controlador;

	public VentanaConfiguraciónConexion(Controlador controlador) 
	{
		super();
		setTitle("Configurar Conexión BD");
		this.controlador = controlador;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 300, 300);
		contentPane = new JPanel();
		this.setResizable(false);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 294, 269);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario: ");
		lblUsuario.setBounds(18, 50, 88, 14);
		panel.add(lblUsuario);
		
		JLabel lblPassword = new JLabel("Contraseña: ");
		lblPassword.setBounds(18, 90, 88, 14);
		panel.add(lblPassword);
	
		JLabel lblIp = new JLabel("Ip: ");
		lblIp.setBounds(18, 130, 88, 14);
		panel.add(lblIp);
		
		JLabel lblPuerto = new JLabel("Puerto");
		lblPuerto.setBounds(18, 170, 88, 14);
		panel.add(lblPuerto);
		
		txtUsuario = new JTextField("");
		txtUsuario.setBounds(113, 48, 164, 20);
		panel.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtPassword = new JTextField("");
		txtPassword.setBounds(113, 88, 164, 20);
		panel.add(txtPassword);
		txtPassword.setColumns(10);
		
		txtIp = new JTextField("");
		txtIp.setBounds(113, 128, 164, 20);
		panel.add(txtIp);
		txtIp.setColumns(10);
		
		txtPuerto = new JTextField("");
		txtPuerto.setBounds(113, 168, 164, 20);
		panel.add(txtPuerto);
		txtPuerto.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 11, 279, 193);
		panel.add(panel_1);
		
		btnConfirmarConfiguracionConexion = new JButton("Confirmar");
		btnConfirmarConfiguracionConexion.setBounds(155, 226, 122, 23);
		panel.add(btnConfirmarConfiguracionConexion);
		btnConfirmarConfiguracionConexion.addActionListener(this.controlador);
		
		btnSalir = new JButton("Salir");
		btnSalir.setBounds(25, 226, 122, 23);
		panel.add(btnSalir);
		btnSalir.addActionListener(this.controlador);
		
		
		this.setVisible(true);
	}
	
	public JButton getBtnConfirmar() 
	{
		return btnConfirmarConfiguracionConexion;
	}
	
	public JButton getBtnSalir()
	{
		return btnSalir;
	}

	public JTextField getTxtUsuario()
	{
		return txtUsuario;
	}

	public JTextField getTxtPassword()
	{
		return txtPassword;
	}

	public JTextField getTxtIp() 
	{
		return txtIp;
	}

	public JTextField getTxtPuerto() 
	{
		return txtPuerto;
	}

	public void setTxtUsuario(JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}

	public void setTxtPassword(JTextField txtPassword) {
		this.txtPassword = txtPassword;
	}

	public void setTxtIp(JTextField txtIp) {
		this.txtIp = txtIp;
	}

	public void setTxtPuerto(JTextField txtPuerto) {
		this.txtPuerto = txtPuerto;
	}

}

