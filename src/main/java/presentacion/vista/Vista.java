package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import javax.swing.JButton;

import persistencia.conexion.Conexion;
import java.awt.BorderLayout;

public class Vista
{
	private JFrame frmAgenda;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnABMLocalidad;
	private JButton btnABMtipoDeContacto;
	private JButton btnConfig;
	private DefaultTableModel modelPersonas;
	private  String[] nombreColumnas = {"Nombre","Apellido","Cumpleaños","Telefono","email","Tipo de Contacto","Calle","Altura","Piso","Depto","Localidad"};

	public Vista() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		frmAgenda = new JFrame();
		frmAgenda.setTitle("Agenda Master 3000");
		frmAgenda.setBounds(100, 100, 1000, 500);
		frmAgenda.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgenda.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		frmAgenda.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 972, 360);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		
		spPersonas.setViewportView(tablaPersonas);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(477, 412, 89, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(576, 412, 89, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(675, 412, 89, 23);
		panel.add(btnBorrar);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setBounds(774, 412, 89, 23);
		panel.add(btnReporte);
		
		btnABMLocalidad = new JButton("ABM - Localidad");
		btnABMLocalidad.setBounds(20, 405, 170, 37);
		panel.add(btnABMLocalidad);
		
		btnABMtipoDeContacto = new JButton("ABM - Tipo De Contacto");
		btnABMtipoDeContacto.setBounds(218, 405, 170, 37);
		panel.add(btnABMtipoDeContacto);
		
		btnConfig = new JButton("Config.");
		btnConfig.setBounds(893, 412, 89, 23);
		panel.add(btnConfig);
	}
	
	public void show()
	{
		this.frmAgenda.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frmAgenda.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frmAgenda.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}


	public JButton getBtnABMLocalidad() {
		return btnABMLocalidad;
	}


	public JButton getBtnABMtipoDeContacto() {
		return btnABMtipoDeContacto;
	}

	public boolean getVisible()
	{
		return this.frmAgenda.isVisible();
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	public JButton getBtnConfig() {
		return btnConfig;
	}
	
	
}
