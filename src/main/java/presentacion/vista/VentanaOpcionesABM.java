package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import presentacion.controlador.Controlador;

public class VentanaOpcionesABM extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private DefaultTableModel modelOpciones;
	private Controlador controlador;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JTable tablaOpciones;
	private String [] nombreColumnas = {"Nombre"};
	
	public VentanaOpcionesABM (Controlador controlador){
		
		super();
		this.controlador = controlador;
	
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 351, 265);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 434, 227);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JScrollPane spOpcionesDisponibles = new JScrollPane();
		spOpcionesDisponibles.setBounds(10, 11, 207, 205);
		panel.add(spOpcionesDisponibles);
		
		modelOpciones = new DefaultTableModel(null, nombreColumnas);
		tablaOpciones = new JTable(modelOpciones){
			private static final long serialVersionUID = 1L;
			public boolean isCellEditable(int rowIndex, int colIndex){return false;}
		};
		spOpcionesDisponibles.setViewportView(tablaOpciones);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(this.controlador);
		btnAgregar.setBounds(233, 48, 89, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(this.controlador);
		btnEditar.setBounds(233, 102, 89, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(this.controlador);
		btnBorrar.setBounds(233, 156, 89, 23);
		panel.add(btnBorrar);
	
		this.setVisible(true);
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}

	public JTable getTablaOpciones() {
		return tablaOpciones;
	}

	public DefaultTableModel getModelOpciones() {
		return modelOpciones;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}
}
