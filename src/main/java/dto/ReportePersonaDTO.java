package dto;

public class ReportePersonaDTO implements Comparable<ReportePersonaDTO>{
	private String apellido;
	private String telefono;
	private String calle;
	private String altura;
	private String piso;
	private String localidad;
	private String fechaNacimiento;
	private String tipoContacto;
	private String email;
	private String direccion;
	// agrupar por mes
	private String mes;
	// ordenar por nombre
	private String nombre;
	private int totales;

	public String getMes() {
		return mes;
	}

//	public void setMes(String mes) {
//		this.mes = mes;
//	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
		this.mes = this.setMes();
	}

	public String getTipoContacto() {
		return tipoContacto;
	}

	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public int getTotales() {
		return totales;
	}

	public void setTotales(int totales) {
		this.totales = totales;
	}

	
	private String setMes(){
		switch(this.intMes()){
		case 1: return "Enero";
		case 2: return "Febrero";
		case 3: return "Marzo";
		case 4: return "Abril";
		case 5: return "Mayo";
		case 6: return "Junio";
		case 7: return "Julio";
		case 8: return "Agosto";
		case 9: return "Septiembre";
		case 10: return "Octubre";
		case 11: return "Noviembre";
		case 12: return "Diciembre";
		default: return null;
		}
	}
	
	public Integer intMes() {
	    //el string de la fecha de nacimiento es dd/mm/yyyy
		return Integer.parseInt(this.fechaNacimiento.substring(3,5));
	}

	@Override
	public int compareTo(ReportePersonaDTO arg0) {
		if(this.getNombre().compareToIgnoreCase(arg0.getNombre()) > 0)
			return 1;			
		else 
			return -1;
	}
}
