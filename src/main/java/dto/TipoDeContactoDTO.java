package dto;

public class TipoDeContactoDTO {
	
	private Integer idTipoDeContacto;
	private String nombre;
	
	public TipoDeContactoDTO(String nombre) {
		this.idTipoDeContacto = null;
		this.nombre = nombre;
	}

	public Integer getIdTipoDeContacto() {
		return idTipoDeContacto;
	}

	public void setIdTipoDeContacto(Integer idTipoDeContacto) {
		this.idTipoDeContacto = idTipoDeContacto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTipoDeContacto == null) ? 0 : idTipoDeContacto.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoDeContactoDTO other = (TipoDeContactoDTO) obj;
		if (idTipoDeContacto == null) {
			if (other.idTipoDeContacto != null)
				return false;
		} else if (!idTipoDeContacto.equals(other.idTipoDeContacto))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	
	@Override
	public String toString() {
		return "TipoDeContactoDTO [nombre=" + nombre + "]";
	}
	
	
}
