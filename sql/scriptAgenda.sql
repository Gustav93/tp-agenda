CREATE DATABASE grupo_15 CHARACTER SET utf8 COLLATE utf8_spanish_ci;

use grupo_15;

Create Table tipoDeContactos(
    idTipoDeContacto Int NOT NULL AUTO_INCREMENT,
    nombre varchar(30) NOT NULL,
    PRIMARY KEY (idTipoDeContacto)
);

Create Table localidades(
    idLocalidad Int NOT NULL AUTO_INCREMENT,
    nombre varchar(30) NOT NULL,
    PRIMARY KEY (idLocalidad)
);

Create Table direcciones(
    idDireccion Int NOT NULL AUTO_INCREMENT,
    calle varchar(30),
    altura varchar(30),
    piso varchar(30),
    depto varchar(30),
    idLocalidad Int NOT NULL,
    FOREIGN KEY (idLocalidad) REFERENCES localidades(idLocalidad),    
    PRIMARY KEY (idDireccion)
);

Create Table personas(
    idPersona Int NOT NULL AUTO_INCREMENT,
    nombre varchar(30),
    apellido varchar(30),
    fechaNac DATE,
    telefono varchar(30),
    email varchar(30),
    idDireccion Int NOT NULL,
    idTipoDeContacto Int NOT NULL,
    FOREIGN KEY (idDireccion) REFERENCES direcciones(idDireccion),
    FOREIGN KEY (idTipoDeContacto) REFERENCES tipoDeContactos(idTipoDeContacto),
    PRIMARY KEY (idPersona)
);

INSERT INTO tipoDeContactos(nombre) VALUES ('Amigo'),('Familia'),('Trabajo');

INSERT INTO localidades(nombre) VALUES ('José C. Paz'),('San Miguel'),('Moreno'),('Tres de Febrero'),('San Martin'),('Polvorines'),('Springfield');

INSERT INTO direcciones(calle,altura,piso,depto,idLocalidad) 
VALUES ('Honduras','5978','1','A',5),
       ('Otawa','4521',null,null,1),
       ('Luis Agote','10385',null,null,3),
       ('Avenida Siempre Viva','723',null,null,7),
       ('Calle Falsa','123',null,null,7);

INSERT INTO personas(nombre,apellido,fechaNac,telefono,email,idDireccion,idTipoDeContacto)
VALUES ('Homero','Simpson','1990-12-01','555-555-555','mmmm@mmmm.com',4,1),
       ('Bart','Simpson','2000-11-02','555-555-555','caguabunga@gmail.com',4,1),

	('Modesto','Rosado','1960-11-11','555-555-555','ejemplo@gmail.com',4,1),
	('Elsa','Pato','1990-06-17','123456-1234','ejemplo@gmail.com',4,1),
	('Jose','Perez','1920-02-21','555-555-555','ejemplo@gmail.com',4,1),
	('Jacinto','Perez','1990-12-01','555-555-555','ejemplo@gmail.com',4,1),
	('Abram','Simpson','1950-03-12','555-555-555','ejemplo@gmail.com',4,1),
	('Otto','null','1990-12-01','555-555-555','ejemplo@gmail.com',4,1),
	('Ash','Ketchum','2000-11-11','555-555-555','ejemplo@gmail.com',4,1),
	('Jeremias','Springfield','1990-12-01','555-555-555','mmmm@mmmm.com',4,1),
	('Lucas','Pratto','1980-06-17','555-555-555','mmmm@mmmm.com',4,1),
	('Franco','Armani','1988-08-23','555-555-555','mmmm@mmmm.com',4,1),
	('German','Lux','1986-02-11','555-555-555','mmmm@mmmm.com',4,1),
	('Rafa','Grogory','1990-12-01','555-555-555','mmmm@mmmm.com',4,1),
	('Leonardo','Ponzio','1985-04-19','555-555-555','mmmm@mmmm.com',4,1),
	('Marge','Buvier','1980-03-08','555-555-555','mmmm@mmmm.com',4,1);